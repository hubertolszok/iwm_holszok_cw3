package lab3;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import Packet.Spectrum;
import Packet.TimeHistory;

public class Klient {
	public static void main(String[] args) {
		long date = System.currentTimeMillis() / 1000;
		var dane1 = new TimeHistory(100, "mm", 1, 480, new Integer[] { 1, 2, 3 }, "device", "dev1", date);
		Dane dataType = Dane.TIMEHISTORY;
		String x = "Zbieranie danych";
		try {
			Registry reg = LocateRegistry.getRegistry(2337);
			Interfejsy serv = (Interfejsy) reg.lookup("Serwer");
			boolean daneRegister = serv.register(dane1);
			System.out.println("Klient wystartowal");
			if (daneRegister) {
				System.out.println("Dane zapisane pomyslnie");
			}

			String data = serv.getRegister(dataType, x);
			System.out.println(data);
			String response = serv.saveObject(Dane.TIMEHISTORY, 1);
			System.out.println(response);
			Object receivedObject = serv.getObject(Dane.TIMEHISTORY, 1);
			System.out.println("Otrzymany obiekt: \n" + receivedObject.toString());

		} catch (RemoteException | NotBoundException e) {
			e.printStackTrace();
		}
	}
}
