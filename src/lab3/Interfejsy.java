package lab3;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface Interfejsy extends Remote {
	boolean register(Object obj) throws RemoteException;

	String getRegister(Dane dataType, String y) throws RemoteException;

	String saveObject(Dane dataType, int rowNr) throws RemoteException;

	Object getObject(Dane dataType, int rowNr) throws RemoteException;
}
