package lab3;

import java.util.ArrayList;
import java.util.List;
import java.rmi.registry.Registry;
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.server.UnicastRemoteObject;
import java.util.Objects;
import Packet.Spectrum;
import Packet.TimeHistory;

public class Serwer implements Interfejsy {
	private final List<TimeHistory> TimeHistoryList = new ArrayList<>();
	private final List<Spectrum> SpectrumList = new ArrayList<>();

	public static void main(String[] args) {

		Registry reg = null;
		try {
			reg = LocateRegistry.createRegistry(2337);
		} catch (RemoteException e) {
			e.printStackTrace();
		}
		try {
			Serwer obj = new Serwer();
			Interfejsy serv = (Interfejsy) UnicastRemoteObject.exportObject(obj, 0);
			assert reg != null;
			reg.bind("Serwer", serv);
			System.out.println("Start");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public Object getObject(Dane dataType, int rowNr) {

		Object obj = null;
		switch (dataType) {
		case TIMEHISTORY:
			if (rowNr <= TimeHistoryList.size() && rowNr > 0) {
				obj = TimeHistoryList.get(rowNr - 1);
			}
			break;
		case SPECTRUM:
			if (rowNr <= SpectrumList.size() && rowNr > 0) {
				obj = SpectrumList.get(rowNr - 1);
			}
			break;
		}
		return obj;
	}

	public String saveObject(Dane dataType, int rowNr) {
		switch (dataType) {
		case TIMEHISTORY:
			if (rowNr <= TimeHistoryList.size() && rowNr > 0) {
				var object = TimeHistoryList.get(rowNr - 1);
				try {
					FileOutputStream fileOut = new FileOutputStream(object.toPath() + ".thi");
					ObjectOutputStream objectOut = new ObjectOutputStream(fileOut);
					objectOut.writeObject(object);
					objectOut.close();
					System.out.println("\nZapisano");
				} catch (Exception ex) {
					ex.printStackTrace();
				}
			} else {
				return "Podaj numer indeksu";
			}
			break;
		case SPECTRUM:
			if (rowNr <= SpectrumList.size() && rowNr > 0) {
				Spectrum object = SpectrumList.get(rowNr - 1);
				try {
					FileOutputStream fileOut = new FileOutputStream(object.toPath() + ".spc");
					ObjectOutputStream objectOut = new ObjectOutputStream(fileOut);
					objectOut.writeObject(object);
					objectOut.close();
					System.out.println("\nZapisano");
				} catch (Exception ex) {
					ex.printStackTrace();
				}
			} else {
				return "Podaj numer indeksu";
			}
			break;
		default:
			return "Podaj list�";
		}
		return "Zapisano" + rowNr + " do pliku";
	}

	public boolean register(Object obj) {
		System.out.println("\nSerwer otrzymal objekt typu: " + obj.getClass() + "\n" + obj.toString());
		boolean wasRegisteredYet = false;
		if (obj.getClass().equals(TimeHistory.class)) {
			for (var object : TimeHistoryList) {
				if ((object.toString()).equals(obj.toString())) {
					wasRegisteredYet = true;
				}
			}
			if (!wasRegisteredYet) {
				TimeHistoryList.add((TimeHistory) obj);
				System.out.println("\nDodano obiekt");
			} else {
				System.out.println("\nTen obiekt juz istnieje");
			}
		} else if (Objects.equals(obj.getClass(), Spectrum.class)) {
			for (var object : SpectrumList) {
				if ((object.toString()).equals(obj.toString()))
					wasRegisteredYet = true;
			}
			if (!wasRegisteredYet) {
				SpectrumList.add((Spectrum) obj);
				System.out.println("\nDodano obiekt");
			} else {
				System.out.println("\nTen obiekt juz istnieje");
			}
		}
		return true;
	}

	public String getRegister(Dane dataType, String y) {
		StringBuilder data;
		boolean containsKeyword;
		if (dataType == Dane.TIMEHISTORY) {
			String[] keywords = y.split("_");
			int iterator = 1;
			data = new StringBuilder("Lista TimeHistory: \n");
			for (var object : TimeHistoryList) {
				containsKeyword = true;
				for (String keyword : keywords) {
					if (!object.toString().toLowerCase().contains(keyword.toLowerCase())) {
						containsKeyword = false;
					}
				}
				if (containsKeyword) {
					data.append(iterator).append(".\n").append(object.toString()).append("\n");
				}
				iterator++;
			}
		} else if (dataType == Dane.SPECTRUM) {
			String[] keywords = y.split("_");
			int iterator = 1;
			data = new StringBuilder("Lista Spectrum: \n");
			for (var object : SpectrumList) {
				containsKeyword = true;
				for (String keyword : keywords) {
					if (!object.toString().toLowerCase().contains(keyword.toLowerCase())) {
						containsKeyword = false;
					}
				}
				if (containsKeyword) {
					data.append(iterator).append(".\n").append(object.toString()).append("\n");
				}
				iterator++;
			}
		} else {
			data = new StringBuilder("Blad");
		}
		return data.toString();
	}
}