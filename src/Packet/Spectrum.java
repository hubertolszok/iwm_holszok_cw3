package Packet;

public class Spectrum <T> extends Sequence {
	public boolean scaling;

	public Spectrum(boolean scaling,String unit, int channelNr, double resolution, T[] buffer ,String device, String description, long date)
	{	
		super(unit,channelNr,resolution,buffer,device,description,date);
		this.scaling = scaling; //true liniowa false logarytmiczna
		
	}

	public String toString()
	{
	return "\nScaling: " + scaling + "\nUnit: " + unit + "\nChannelNr: " + channelNr + "\nResolution: " + resolution + "\nBuffer: " + buffer[0] + "\nDevice: " + device + "\nDescription: " + description + "\nDate: " + date;
	}

}
